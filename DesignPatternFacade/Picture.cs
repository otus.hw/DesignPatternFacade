﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DesignPatternFacade
{
    /// <summary>
    /// Изменение размеров исходной картинки под фиксированный размер мотиватора
    /// </summary>
    public static class Picture
    {
        public static Bitmap Resize(int width, int height, Image image)
        {
            var max = Math.Min((decimal)width / (decimal)image.Width, (decimal)height / (decimal)image.Height);

            Bitmap b = new Bitmap((int)(image.Width * max), (int)(image.Height * max));
            using (Graphics g = Graphics.FromImage(b))
            {
                g.Clear(Color.White);
                g.DrawImage(image, new RectangleF(3, 3, b.Width-6, b.Height-6));
            }
            return b;
        }
    }
}
