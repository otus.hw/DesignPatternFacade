﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DesignPatternFacade
{
    /// <summary>
    /// Подготовка фона и вставка текста
    /// </summary>
    public static class Background
    {
        
        public static Bitmap CreateBackground(int width, int height, string text, int textArea)
        {

            var background = new Bitmap(width,height+ textArea);
            using (Graphics g = Graphics.FromImage(background))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                g.Clear(Color.Black);

                Rectangle rectangle = new Rectangle(0, height, width, textArea);
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.FillRectangle(Brushes.Black, rectangle);

                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;

                g.DrawString(text, new Font("Tahoma", 18), Brushes.White, rectangle,sf);
                g.Flush();
            }
            return background;
        }
    }
}
