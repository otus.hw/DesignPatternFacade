﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace DesignPatternFacade
{
    /// <summary>
    /// Создание мотиватора из поготовленного фона и картинки
    /// </summary>
    public static class MakeMotivator
    {
        public static Bitmap Make(Bitmap background, Image image, int textArea)
        {
            using (Graphics g = Graphics.FromImage(background))
            {
                g.DrawImage(background, 0, 0);

                g.DrawImage(image, (background.Width- image.Width)/2, (background.Height-image.Height-textArea)/2);
            }
            return background;
        }
    }
}
