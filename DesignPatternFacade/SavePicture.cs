﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DesignPatternFacade
{
    /// <summary>
    /// Сохранение картинки
    /// </summary>
    public static class SavePicture
    {
        public static void Save(Image image, string filepath)
        {
            image.Save(filepath, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
    }
}
