﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DesignPatternFacade
{
    class Program
    {
        static void Main(string[] args)
        {
            var text = "ert gfdbhdfn sdfghdfg sdfg sfdg ssdfg sdfg";

            var generator = new MotivatorGenerator();

            IEnumerable<string> files = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory).Where(s => s.EndsWith(".jpeg") || s.EndsWith(".jpg"));
            foreach (var file in files)
            {
                generator.CreateMotivator(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,file), text);
            }

            Console.WriteLine("Hello World!");
        }
    }
}
