﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace DesignPatternFacade
{
    /// <summary>
    /// генератор мотиваторов
    /// </summary>
    public class MotivatorGenerator
    {
        // разрешение картинки мотиватора
        int _width = 800;
        int _height = 640;
        //Высота прямоугольника под текст 
        int _textAreaHeight = 150;
        // минимальный отступ 
        int _padding = 25;

        string _filename;

        public string CreateMotivator(string path, string text)
        {
            _filename = Path.GetFileName(path);
            // загружаем картинку
            var image = Bitmap.FromFile(path);

            //изменяем рамер картинки под заданое разрешение
            image = Picture.Resize(_width - _padding*2, _height - _padding*2, image);

            // делаем фон и вставляем тескт
            var background = Background.CreateBackground(_width,_height, text,_textAreaHeight);

            // склеиваем фон и подготовленную картинку
            image = MakeMotivator.Make(background,image, _textAreaHeight);

            // Сохраняем в файл
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _filename + "-motiv.jpeg");
            SavePicture.Save(image, filePath);

            return filePath;
        }


    }
}
